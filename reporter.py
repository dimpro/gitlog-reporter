#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import sys
import re
import getopt


class GitLogReporter:
    class GitTag:
        def __init__(self, commit_hash='', name='', contents=''):
            self.commit_hash = commit_hash
            self.name = name
            self.contents = contents

    class GitCommit:
        def __init__(self, commit_hash='', date='', subject='', body=''):
            self.commit_hash = commit_hash
            self.date = date
            self.subject = subject
            self.body = body
            self.tags = []

        @staticmethod
        def __replace_html_symbols(text='') -> str:
            return text.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;')

        def get_html(self) -> str:
            ret_str = '<u><b>{}</b></u><br>{}'.format(
                self.__replace_html_symbols(self.date),
                self.__replace_html_symbols(self.subject)
            )
            if self.body is not '':
                ret_str += '<br>{}'.format(self.__replace_html_symbols(self.body).replace('\n', '<br>'))
            # print(len(self.tags))
            for tag in self.tags:
                ret_str += '<br><b>tag:</b> {}'.format(self.__replace_html_symbols(tag.name))
                if tag.contents is not '':
                    if re.fullmatch('^<script>.*</script>$', tag.contents) is not None:
                        ret_str += tag.contents
                    else:
                        ret_str += '<br>{}'.format(self.__replace_html_symbols(tag.contents))
            return '{}<br><br>'.format(ret_str)

    # === self class ===

    def __init__(self):
        self.__dir = ''
        self.__template_file_name = ''
        self.__replace_string = ''
        self.__output_file_name = ''
        self.__commit_range = ''
        self.__tag_regex = ''

        self.__b_dir = False
        self.__b_template_file_name = False
        self.__b_replace_string = False
        self.__b_output_file_name = False
        self.__b_commit_range = False
        self.__b_tag_regex = False

        self.__script_dir = os.path.dirname(os.path.abspath(__file__))
        self.__b_print_range = False

    @staticmethod
    def __str_to_list(text='', sep=''):
        return [x.strip() for x in text.strip().split(sep) if len(x)]

    @staticmethod
    def git_is_current_branch(commit_hash):
        if os.popen(
                'git branch --format="%(if)%(HEAD)%(then)ok%(end)" --contains {}'.format(commit_hash)
        ).read().strip():
            return True
        else:
            return False

    def git_tags(self) -> list:
        format_str = \
            '%(refname:short)[|]' \
            '%(if:equals=commit)%(objecttype)%(then)' \
            '%(objectname:short)[|]' \
            '%(else)' \
            '%(*objectname:short)[|]%(contents)' \
            '%(end)[n]'
        cmd = 'git for-each-ref refs/tags --sort=creatordate --format="{}"'.format(format_str)
        tags = []
        for tag_str in self.__str_to_list(os.popen(cmd).read(), '[n]'):
            tag = tag_str.split('[|]')
            tags.insert(0, self.GitTag(name=tag[0], commit_hash=tag[1], contents=tag[2]))
        return tags

    def git_commits(self) -> list:
        format_str = '%h[|]%cd[|]%s[|]%b[n]'
        cmd = 'git log --date=short --pretty=format:"{}" {}'.format(format_str, self.__commit_range)
        commits = []
        for commit_str in self.__str_to_list(os.popen(cmd).read(), '[n]'):
            commit = commit_str.split('[|]')
            commits.append(self.GitCommit(commit_hash=commit[0], date=commit[1], subject=commit[2], body=commit[3]))
        return commits

    def git_log(self) -> list:
        tags = self.git_tags()

        if self.__b_tag_regex:
            i = 0
            while i < len(tags):
                if self.git_is_current_branch(tags[i].commit_hash):
                    i += 1
                else:
                    del tags[i]

            finded = []
            for tag in tags:
                if re.fullmatch(self.__tag_regex, tag.name) is not None:
                    finded.append(tag.name)
                    if len(finded) == 2:
                        break
            if len(finded) == 1:
                self.set_range(finded[0])
            elif len(finded) == 0:
                self.set_range('HEAD')
            else:
                self.set_range('..'.join(list(reversed(finded))))

        commits = self.git_commits()

        i = 0
        while i < len(tags):
            found = False
            for ci in commits:
                # print(i, tags[i].name, ci.subject)
                if tags[i].commit_hash == ci.commit_hash:
                    # print(tags[i].commit_hash, tags[i].name, tags[i].contents)
                    found = True
                    ci.tags.append(tags[i])
                    del tags[i]
                    break
            if not found:
                i += 1

        return commits

    def commits_to_html(self, commits) -> str:
        ret_str = ''
        if self.__b_print_range:
            ret_str += self.__commit_range + '<br><br>\n'

        for commit in commits:
            ret_str += commit.get_html() + '\n'

        return ret_str

    def create_output_file(self, text=''):
        if self.__b_template_file_name:
            try:
                f = open(self.__template_file_name, 'r')
            except IOError:
                raise Exception('ERR: cant open ' + self.__template_file_name)
            templ = f.readlines()
            f.close()

            indexof_replace_string = -1
            indexof_body = -1
            for i in range(len(templ)):
                current_str = templ[i].strip()

                if self.__b_replace_string:
                    if current_str == self.__replace_string:
                        indexof_replace_string = i
                if current_str == '</body>':
                    indexof_body = i

            if self.__b_replace_string and indexof_replace_string != -1:
                templ[indexof_replace_string] = text
            elif indexof_body != -1:
                templ.insert(indexof_body, text)
            else:
                templ.append(text)
        else:
            templ = [text]

        if self.__b_output_file_name:
            output_file_name = self.__output_file_name
        else:
            output_file_name = self.__script_dir + '/report.html'

        try:
            f = open(output_file_name, 'w')
        except IOError:
            raise Exception('ERR: cant write ' + output_file_name)

        f.writelines(templ)
        f.close()

    # === settings ===

    def set_dir(self, path):
        self.__b_dir = True
        self.__dir = os.path.abspath(path)

    def set_template(self, path):
        self.__b_template_file_name = True
        self.__template_file_name = os.path.abspath(path)

    def set_replace_string(self, rep_str):
        self.__b_replace_string = True
        self.__replace_string = rep_str

    def set_output_file(self, path):
        self.__b_output_file_name = True
        self.__output_file_name = os.path.abspath(path)

    def set_range(self, range_str):
        self.__b_commit_range = True
        self.__commit_range = range_str

    def set_regex(self, regex):
        self.__b_tag_regex = True
        self.__tag_regex = regex

    def set_print_range(self):
        self.__b_print_range = True

    def apply_opts(self):
        if not self.__b_dir or not (self.__b_commit_range ^ self.__b_tag_regex):
            raise Exception('ERR: incorrect options')

        if self.__b_template_file_name and not os.path.isfile(self.__template_file_name):
            raise Exception('ERR: incorrect template file name')

        if not os.path.isdir(self.__dir):
            raise Exception('ERR: incorrect project directory')

        if not os.path.isdir(self.__dir + '/.git'):
            raise Exception('ERR: .git not exist')

        os.chdir(self.__dir)


def usage():
    print('usage: reporter -d DIR (-r RANGE | -e REGEX) [-t TEMPLATE] [-s REPLACE_STRING] [-o FILE] [-R] [-h]')
    print()
    print('-d is required and either of -r or -e is required too')
    print()
    print('-d project dir with git')
    print('-r range of commit hashes or tags')
    print('-e regular expression to find the two last tags and use it as a range')
    print('-t template file which will be used for inserting the text in it and creating the html')
    print('-s string which will be find in the template and will replace it by the git log')
    print('-o output file which will be created from template file')
    print('-R print the range in the output file')
    print('-h this help')


if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hd:t:s:o:r:e:R')
    except getopt.GetoptError:
        usage()
        exit()

    gitLogReporter = GitLogReporter()

    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit()
        elif opt == '-d':
            gitLogReporter.set_dir(arg)
        elif opt == '-t':
            gitLogReporter.set_template(arg)
        elif opt == '-s':
            gitLogReporter.set_replace_string(arg)
        elif opt == '-o':
            gitLogReporter.set_output_file(arg)
        elif opt == '-r':
            gitLogReporter.set_range(arg)
        elif opt == '-e':
            gitLogReporter.set_regex(arg)
        elif opt == '-R':
            gitLogReporter.set_print_range()

    try:
        gitLogReporter.apply_opts()
    except Exception as e:
        print(e)
        print()
        usage()
        exit()

    commits = gitLogReporter.git_log()
    html_insertion = gitLogReporter.commits_to_html(commits)
    gitLogReporter.create_output_file(html_insertion)

    exit()

